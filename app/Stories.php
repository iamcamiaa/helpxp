<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stories extends Model
{
    //

    protected $primaryKey = 'storyid';
    public function patient(){
        return $this->hasOne('App\Patient');
    }

    public function picture(){
        return $this->hasMany('App\Picture', 'storyid', 'storyid');
    }
    
}
