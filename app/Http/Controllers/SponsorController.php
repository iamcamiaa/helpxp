<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Sponsor;
use App\Patient;
use App\User;
use App\Donation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class SponsorController extends Controller
{
      
  public function newSponsor($patientid){
        $patient = Patient::findorfail($patientid);
      return view('sponsorDonate')->with(['patient'=>$patient]);
    }

    public function saveSponsor(Request $request){ 
      $patient = Patient::findorfail($request->patientid);
      $lacking = $patient['goal'] - $patient['donations'];
      $amount = $request->amount;
      $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();
      $total = $sponsor->sum('voucherValue'); 
      $collect = new Collection();
      // foreach($sponsor as $s){
      //   $val = $s;
      //   $collect->push($val);
      // }
      $V = new Collection();
      $amt = $amount;
if($amt <= $total){
        foreach($sponsor as $c){ 
          if($amt - $c['voucherValue'] >= 0){
            $V->push($c);
            // $s = Sponsor::find($c['sponsor_serial']);
            // $s->status = "donated";
            // $s->save();
            $amt -= $c['voucherValue'];
          }
        }
      }

      //   $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'asc')->get();
      //   $collect = new Collection();
      // // foreach($sponsor as $s){
      // //   $val = $s;
      // //   $collect->push($val);
      // // }
      //   while ($amt > 0) {
      //     foreach($sponsor as $c){ 
      //       while ($amt > 0) {
      //       // $V->push($c);
      //       // $s = Sponsor::find($c['sponsor_serial']);
      //       // $s->status = "donated";
      //       // $s->save();
      //       $amt -= $c['voucherValue'];
      //     }
      //   }
      //   }
        // return $amt;
        
// if($amt < 0){

//   return view('donateAny')->with(['amt'=>$amt]);
// }
       // return $V;

        $avblVoucher =  $V->sum('voucherValue');

      if($amt == 0){
        $total = $patient->TotalRedeem + $V->sum('voucherValue');
        $patient->TotalRedeem = $total;
        $patient->save();
          foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "donated";
          $s->save();
          $donation = new Donation();
          $donation->patientid = $patient->patientid;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->save();
        }
          return Redirect::back()->with('success', true); 
        }else if ($avblVoucher != 0){      
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }else
          return Redirect::back()->with('alert', true);

        
         
// }else
// return "dili pwede";

//     foreach ($V as $donate){
//           $donation = new Donation();
//           $donation->patientid = $patient->patientid;
//           $donation->sponsor_serial = $donate['sponsor_serial'];
//           $donation->save();
//     }

        //  $pnt = Patient::get();

        //  $data = [];
        //  foreach($pnt as $p){          
        //         $count = 0;
        //             if($p['status'] != null){  
        //                   $count++;

        //             }
        //             if($p['goal'] == $p['TotalRedeem']){
        //                 $count++;
                        
        //             }
                
        //     if($count == 0)
        //     array_push($data, $p);
        //  }
        // return view('homepage')->with(['data'=>$data]);   
    }














    public function saveSponsorAny(Request $request){
      $amount = $request->amount;
      $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();
      $total = $sponsor->sum('voucherValue'); 
      $collect = new Collection();
      // foreach($sponsor as $s){
      //   $val = $s;
      //   $collect->push($val);
      // }
      $V = new Collection();
      $amt = $amount;
if($amt <= $total){
        foreach($sponsor as $c){ 
          if($amt - $c['voucherValue'] >= 0){
            $V->push($c);
            // $s = Sponsor::find($c['sponsor_serial']);
            // $s->status = "donated";
            // $s->save();
            $amt -= $c['voucherValue'];
          }
        }

      //   $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'asc')->get();
      //   $collect = new Collection();
      // // foreach($sponsor as $s){
      // //   $val = $s;
      // //   $collect->push($val);
      // // }
      //   while ($amt > 0) {
      //     foreach($sponsor as $c){ 
      //       while ($amt > 0) {
      //       $V->push($c);
      //       $s = Sponsor::find($c['sponsor_serial']);
      //       $s->status = "donated";
      //       $s->save();
      //       $amt -= $c['voucherValue'];
      //     }
      //   }
      //   }
        // return $amt;
}        
// if($amt < 0){

//   return view('donateAny')->with(['amt'=>$amt]);
// }
        $avblVoucher =  $V->sum('voucherValue');
//$request->session()->flash('alert-success', 'are you sure?');
        if($amt == 0){
          foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "donated";
          $s->save();
          $donation = new Donation();
          $donation->patientid = null;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->save();
        }
          return Redirect::back()->with('success', true); 
        }else if ($avblVoucher != 0){    
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }else
          return Redirect::back()->with('alert', true);

        

 

        // foreach ($V as $donate){
        //   $s = Sponsor::find($donate['sponsor_serial']);
        //   $s->status = "donated";
        //   $s->save();
        //   $donation = new Donation();
        //   $donation->patientid = null;
        //   $donation->sponsor_serial = $donate['sponsor_serial'];
        //   $donation->save();
        // }

      
       //   $pnt = Patient::get();

       //   $data = [];
       //   foreach($pnt as $p){          
       //          $count = 0;
       //              if($p['status'] != null){  
       //                    $count++;

       //              }
       //              if($p['goal'] == $p['TotalRedeem']){
       //                  $count++;
                        
       //              }
                
       //      if($count == 0)
       //      array_push($data, $p);
       //   }
       // return view('homepage')->with(['data'=>$data]);
    }

   
    public function newSponsorAny(){
         return view('donateAny');
    }


}
