@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Patient</div>
                <div class="card-body">

<table>
	<tr>
		<td><h4><img src="{{  url('storage/picture/'.$patient->filename)}}" width="200px" height="150px" /></h4></td>
	</tr>
	<tr>
		<td><h2>{{$story['storytitle']}}</h2></td>
	</tr>
	<tr>
		<td><h4>{{$story['story']}}</h4></td>
	</tr>           
 
	<tr>
		<td>beneficiary name:</td>
	</tr>
	<tr>
		<td><h4> {{$patient->patientname}}</h4></td>
	</tr>

	<tr>
		<td>Contact info:<h4> {{$patient->userName->email}}</h4></td>
		<td><h4>+63{{$patient->userName->contact}}</h4></td>
	</tr>
	<tr>
		<td>illness:</td>
	</tr>
	<tr>
		<td><h4>{{$patient['illness']}}</h4></td>
	</tr>
	<tr>
		<td>amount needed:</td>
	</tr>
	<tr>
		<td><h4>{{$patient['goal']}}</h4></td>
	</tr>
	<tr>
		<td>Requirements:</td>
	</tr>
	
	<tr>
	@foreach($pic as $picture)
		<td><h4><img src="{{  url('storage/picture/'.$picture->filename)}}" width="200px" height="150px" /></h4></td>
	@endforeach
	</tr>
	
</table>

<br><br>
<h2>{{$patient->goal - $patient->TotalRedeem}} out of {{$patient->goal}}</h2>
<div class="progress">
    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{$patient->TotalRedeem}}" aria-valuemin="0" aria-valuemax="{{$patient->goal}}" style="width:{{$patient->TotalRedeem - $patient->goal}}%">
      {{$patient->TotalRedeem}}
    </div>
  </div>
    <p style="float: left">P{{$patient->TotalRedeem}}</p>
  <p style="float: right">Goal: P{{$patient->goal}}</p>
<br><br>
<p style="float: right;">@include('navback')</p>
@if(Auth::id() == $patient['userid'])
<a href="/update/{{$patient['patientid']}}">Add update</a>
@endif

 </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
 <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Updates</div>
                <div class="card-body">

<table>
@foreach ($ups as $u)
<tr>
<td>title: {{$u->storytitle}}</td>
</tr>
<tr><td>story: {{$u->story}}</td></tr>
<tr>
@foreach ($u->picture as $update)
<td><h4><img src="{{  url('storage/picture/'.$update->filename)}}" width="200px" height="150px" /></h4></td>
@endforeach
</tr>
@endforeach
</table>

 </div>
            </div>
        </div>
    </div>
</div>
@endsection