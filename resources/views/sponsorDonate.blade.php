@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Donate to this Patient</div>
                <div class="card-body">
<form  action="{{url('/helpxp')}}" method="post">
	{{csrf_field()}}


<table>
	<tr>
		<td><input type="hidden" name="patientid" value="{{ $patient->patientid}}"></td>
	</tr>
	<tr>
		<td>Name of the Donee:</td>
		<td>{{ $patient->userName->name}}</td>
	</tr>

	<tr>
		<td>Amount to be donated:</td>
		<!-- <td><input type="text" name="amount" size="50"></td> -->
		<td>
		<input type="text" name="amount" id="amount" size="10">
		</td>
	</tr>
	
</table><br>
<center><input type="submit" class="btn btn-primary" name="submit" onclick="check()"  value="DONATE"></center>
</form>
</div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="lacking" value="{{$patient['goal'] - $patient['donations']}}">


@if(Session::has('info'))
<div class="alert alert-confirm">
    <script>
    	confirm('You cant donate your desire amount, because of your lacking denomination. However you can donate {{ Session::get('avblVoucher', '') }}.');
    </script>  
</div>
@elseif(Session::has('alert'))
	<script>
		confirm('You cant donate your desire amount, because of your lacking denomination.');
	</script>

@elseif(Session::has('success'))
	<script>
		alert('Successful Donation');
	</script>
@endif



@endsection

