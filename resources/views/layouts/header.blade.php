<?php 
use App\Patient;
?>

<!DOCTYPE html>
<html>
<head>
  <title>HELPXP</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/datepicker3.css" rel="stylesheet">
  <link href="css/styles.css" rel="stylesheet">
   <link href="{{ asset('css/jumbotron.css') }}" rel="stylesheet">
   <link href="{{ asset('css/nav.css') }}" rel="stylesheet">
  <!--Custom Font-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    
</head>
<body>

<ul>
  @guest
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span></button>
        <a class="navbar-brand" href="{{ url('/') }}"><span>Help</span>XP</a>       
          <li style="float: right; text-decoration: none;"><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
            <li style="float: right; text-decoration: none;"><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
      </div>
    </div>
  </nav>
  @else
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span></button>
        <a class="navbar-brand" href="{{ url('/') }}"><span>Help</span>XP</a>


<?php
$user = Auth::id();
$patient = Patient::where('userid', $user)->where('status', '=', null)->get()->count(); 
?>
    @if($patient <= 0)
  <li><a href="{{ url('/patientsdetail') }}">Apply</a></li>
    @else
    @endif


  <li><a href="{{ url('/donateAny') }}/{{Auth::user()->id}}">Raise Funds</a></li>
  <li><a href="{{ url('/buyvoucher') }}/{{Auth::user()->id}}">Buy Voucher</a></li>
  <li><a href="{{ url('/about') }}">About Us</a></li>
  <li style="float:right"><a href="{{ route('logout')}}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
      {{ __('Logout') }}</a></li>                           
  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
  <li style="float:right"><a href="{{ url('/history') }}">My History</a></li>
  <li style="float:right"><a href="{{ url('/') }}">Hello {{ Auth::user()->username }}!</a></li>
  @endguest

        </div>
    </div>
  </nav>
</ul>


 <main class="py-4">
            @yield('content')
        </main>
        


</body>
</html>
   