@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">What's the update?</div>
                <div class="card-body">
<form action="{{url('/saveUpdate')}}" method="post" enctype="multipart/form-data">
{{csrf_field()}}
<table>
<input type="hidden" name="patientid" value="{{$patient}}">
	<tr>
		<th width="30%">Update Title:</th>
		<td><input type="text" name="updatetitle" size="60"></td>
	</tr>
	<tr>
		<th width="30%">Post Story:</th>
		<td><textarea type="text" name="story" rows="4" cols="60"></textarea></td>
	</tr>
	<tr>
		<th width="30%">Add a photo </th>
		<td><input type="file" name="file[]" size="60" multiple></td>
	</tr>
</table><br>
<center><input type="submit" class="btn btn-primary" value="Submit">&nbsp; &nbsp;
		<input type="reset" class="btn btn-danger" value="Reset"></center>
</form>

<p style="float: right;">@include('navback')</p>


 </div>
            </div>
        </div>
    </div>
</div>



@endsection