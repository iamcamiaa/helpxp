@extends('layouts.app')

 
@section('content')

 <div class="about" style="height: auto">
  <div class="desc" style="background-color: lightblue; border: 1px solid #ccc">
	 <h3><u>BUY VOUCHER</u></h3>
	 <p>HelpXP's voucher indicates that you are allowed to donate to a specific patient or just donating without any cause.</p><p style="font-weight: bold;">NOTE! &nbsp; You cannot donate without availing vouchers.</p>  
  </div>
</div> 


<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
<div class="card">
<div class="card-header">Buy Voucher</div>
<div class="card-body">
<form action="{{url('/voucher')}}" method="post">
{{csrf_field()}}
<table>
	<tr>
		<td><input type="checkbox" name="check100" value="100">100</td>
		<td><input type="range" min="0" max="100" name="slider100"></td>
	<!-- 	<th width="30%">Amount:</th>
		<td><input type="text" name="amount" size="30"></td> -->
	</tr>
	<tr>
		<td><input type="checkbox" name="check500" value="500">500</td>
		<td><input type="range" min="0" max="100" name="slider500"></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="check1000" value="1000">1000</td>
		<td><input type="range" min="0" max="100" name="slider1000"></td>
	</tr>
	<tr>
		<td><input type="checkbox" name="check5000" value="5000">5000</td>
		<td><input type="range" min="0" max="100" name="slider5000"></td>
	</tr>

</table><br>
<center><input type="submit" class="btn btn-primary" value="Submit">&nbsp; &nbsp;
		<input type="reset" class="btn btn-danger" value="Reset"></center>
</form>
</div>
</div>
</div>
</div>
</div>

<br>

 <div class="others">
  <div class="desc" style="float: left">
  </div>
</div> 
@endsection 