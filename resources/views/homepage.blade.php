@extends('layouts.nav')
@section('content')
<?php 
use App\Sponsor;
$sponsor = Sponsor::where('userid', Auth::id())->where('status', null)->get();


?>

<div class="container">
  <div class="container">
 <div class="gallery">
  <div class="desc" style="color: green"> 
   <h1>P{{DB::table('patients')->sum('TotalRedeem')}} all time donations</h1>
  </div>
</div> 
</div>
</div>

<br><br><br>
<div class="container">
<h1>Stories</h1>
 @foreach ($data as $patients)
   <div class="container">

<div class="gallery">
    <div class="desc">
    <table>
    <tr>
      <td>Title:</td>
      <td><h3>{{$patients->title}}</h3></td>
    </tr>
    <tr>
      <td>Name:</td>
      <td><p>{{$patients->userName->name}}</p></td>
    </tr>
    <tr>
    <td>Lacking:</td>
    <td><p>{{$patients->goal - $patients->TotalRedeem}}</p></td>
    </tr>

    <tr>
      <td> <p><a href="http://localhost:8000/list/{{$patients['patientid']}}/view">View Details</a></p></td><td></td>

      @if ($sponsor == null )
      <td><p><a href="http://localhost:8000/buyvoucher/{{Auth::id()}}">Buy Voucher</a></p></td>
      @else
      <td><p><a href="http://localhost:8000/sponsorDonate/{{ $patients['patientid'] }}">Donate</a></p></td>
      @endif
    </tr>
    </table>

   <!--   angel -->
<div class="progress">
    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{$patients->TotalRedeem}}" aria-valuemin="0" aria-valuemax="{{$patients->goal}}" style="width:{{$patients->TotalRedeem - $patients->goal}}%">
      {{$patients->TotalRedeem}}
    </div>
  </div>
    <p style="float: left">P{{$patients->TotalRedeem}}</p>
  <p style="float: right">Goal: P{{$patients->goal}}</p>
  </div>
  </div>
</div>
    @endforeach
   
  </div>


@endsection







