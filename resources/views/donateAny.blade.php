@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Donate to HelpXP</div>
                <div class="card-body">

<!-- @if($errors->any())
<p>{{$errors->first()}}</p>
@endif -->


<form action="{{url('/homepage')}}" method="post">
	{{csrf_field()}}

<table>

	<tr>
		<td>Amount to be donated:</td>
		<td>
		<!-- <input type="radio" name="goal" id="goal" value="500">500
		<input type="radio" name="goal" id="goal" value="1000">1000
		<input type="radio" name="goal" id="goal" value="3000">3000
		<input type="radio" name="goal" id="goal" value="5000">5000 -->
		<input type="text" name="amount" id="amount" size="10">
		</td>

	</tr>

</table><br>
<center><input type="submit" class="btn btn-primary" name="submit"  value="DONATE"></center>
</form>


@if(Session::has('info'))
<div class="alert alert-confirm">
    <script>
    	confirm('You cant donate your desire amount, because of your lacking denomination. However you can donate {{ Session::get('avblVoucher', '') }}.');
    </script>  
</div>
@elseif(Session::has('alert'))
	<script>
		confirm('You cant donate your desire amount, because of your lacking denomination.');
	</script>
@elseif(Session::has('success'))
	<script>
		alert('Successful Donation');
	</script>
@endif


</div>
            </div>
        </div>
    </div>
</div>

<!-- <script>
function check(){
	var voucher = document.getElementById("voucher").value;
	var gl = document.getElementById("gl").value;
	var goal = document.getElementById("goal").value;

if(gl > voucher){
	alert("greater");
}
	 // if(gl < 200)
	 // 	alert("minimum of 200");

	//  if(voucher < gl)
	// 	alert("Your donation is greater");
	// else
	// 	alert("success");
	
}
</script>
 -->

<!-- <script>   
		alert("donation has a remaining balance");
</script> -->

@endsection

