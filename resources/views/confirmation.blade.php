<h1>HELPXP</h1>
Hello,
<p>Thank you for registering your HelpXP Account. To finally activate your account please click the following link.</p>
{{ route('confirmation', $token)}}
<br><br><br><br>
<p>Regards,</p>
<p>Your HELPXP Team</p>
<p>----------------</p>
<p>www.helpxp.com</p>

<br><br><br>
<p>Tel. +639430620233 </p>