 @extends('layouts.app')

@section('content')

<div class="about" style="width: 25%; height: auto">
  <div class="desc" style="background-color: lightgreen; border: 1px solid #ccc">
	 requirements:
	 <br>original copy of medical abstract
	 <br>medical certification
	 <br>Valid ID's (voter's ID, passport, etc.)
	 <br>hospital bill
	 <br>official quotation of breakdown of expenses
  </div>
</div> 

<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
<div class="card">
<div class="card-header">Enter your goal here!</div>
<div class="card-body">
<form action="{{url('/singlelist')}}" method="post" enctype="multipart/form-data">
{{csrf_field()}}
<table>
	<tr>
		<th width="30%">Goal Title:</th>
		<td><input type="text" name="title" size="60"></td>
	</tr>
	<tr>
		<th width="30%">Post Story:</th>
		<td><textarea type="text" name="story" rows="4" cols="60"></textarea></td>
	</tr>
	<tr>
		<th width="30%">Goal:</th>
		<td><input type="text" name="goal" size="60"></td>
	</tr>
	<tr>
		<th width="30%">Add a profile goal: </th>
		<td><input type="file" name="profile" size="60"></td>
	</tr>
	<tr>
		<th width="30%">Add a photo (serve as documents): </th>
		<td><input type="file" name="file[]" size="60" multiple></td>
	</tr>
	<tr>
		<th width="30%">Beneficiary name:</th>
		<td><input type="text" name="bname" size="60"></td>
	</tr>
	<tr>
		<th width="30%">Illness:</th>
		<td><input type="text" name="illness" size="60"></td>
	</tr>
</table><br>
<center><input type="submit" class="btn btn-primary" value="Submit">&nbsp; &nbsp;
		<input type="reset" class="btn btn-danger" value="Reset"></center>
</form>
</div>
</div>
</div>
</div>
</div>
@endsection 