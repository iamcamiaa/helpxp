@extends('layouts.app')
@section('content')


<h1 style="text-align: center">ABOUT HELPXP</h1>

<div class="container">
   <div class="container">

 <div class="gallery">
  <div class="desc" style="background-color: lightblue">
  	<p>HelpXP is an online fundraising tool which aims to help indigent people in applying for help in paying hospital bills.</p>
  </div>
</div> 
	<br><br><br><br>
<h2 style="text-align: center">HOW HELPXP WORKS</h2>
 <div class="about">
  <div class="desc">
	 <h3>PATIENT</h3>
	 <h4>Step up for a life challenge.</h4><br><p>
	 	Start fundraise for your hospital needs by just posting your story including the amount of money needed. </p><br>
	 	<p>
	 	Step 1: Create an account. <br>
	 	Step 2: After creating an account, apply for help. <br>
	 	Step 3: Create your story and fill up the necessary information. Then post it! <br>
	 	Step 4: Your story will now be posted to our story page and is open for donations. <br>
	 	</p>
  </div>
</div> 

<div class="about">
  <div class="desc">
	 <h3>DONOR</h3>
	 <h4>Help out a mate in need.</h4><br>
	 <p>You can raise funds to support one's medical treatment by simply donating to that specific patient/person or by buying vouchers to our system. HelpXP vouchers are another way to give a little love.</p><br>
	 	<p>
	 	Step 1: Create an account. <br>
	 	Step 2: After creating an account, buy voucher to our system. <br>
	 	Step 3: Select a patient you wanted to help. <br>
	 	Step 4: You can now donate to any person.<br>
	 	</p>
  </div>
</div>

  </div>
</div>



@endsection


