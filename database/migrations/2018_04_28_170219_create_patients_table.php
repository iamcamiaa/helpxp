<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('patientid');
            $table->string('userid');
            $table->integer('goal');
            $table->string('filename');
            //$table->integer('donations')->nullable();
            $table->string('status')->nullable();
            $table->integer('TotalRedeem')->nullable();
            $table->date('expirydateV')->nullable();
            $table->string('patientname');
            $table->string('illness');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
